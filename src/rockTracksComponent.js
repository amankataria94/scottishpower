import React from 'react';
import Proptypes from 'prop-types';
import { connect } from 'react-redux'
import TrackListComponent from './trackListComponent';
import TrackComponent from './trackComponent';

class RockTracksComponent extends React.Component {
    render() {
        if(this.props.selectedTrack) {
            return <TrackComponent track={this.props.selectedTrack}/>;
        }
        else {
            return (
                <TrackListComponent api={this.props.api}/>
            )
        }
    }
}

RockTracksComponent.Prototypes = {
    api: Proptypes.object.isRequired 
}

const mapStateToProps = (state) => {
    return {
        selectedTrack: state.selectedTrack.selected
    }
}

export default connect(mapStateToProps)(RockTracksComponent)