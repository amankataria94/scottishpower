export default function(state = [], action) {
    switch(action.type) {
        case 'SET_SELECTED':
            return Object.assign({}, state, {
                selected: action.track
            })
        default:
            return state
    }
}