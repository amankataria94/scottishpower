import { combineReducers } from 'redux';
import selectedTrack from './selectTrackReducer';

export default combineReducers({
    selectedTrack
})