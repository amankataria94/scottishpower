import jsonp from 'jsonp';

export default class API {
    fetchTrackList() {
        return new Promise((resolve, reject) => {
            jsonp('https://itunes.apple.com/search?term=rock&media=music', null, (err, data) => {
                if(err) {
                    reject('Failed to fetch');
                }
                else {
                    resolve(data)
                }
            });
        })
    }
}