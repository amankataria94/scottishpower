import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore } from 'redux'
import reducer from './reducers/index'
import './App.css';
import API from './api';
import RockTracksComponent from './rockTracksComponent'

class App extends Component {
  constructor(props) {
    super(props);
    this.store = createStore(reducer);
    this.api = new API();
  }

  render() {
    return (
      <div className="App">
      <Provider store={this.store}>
        <RockTracksComponent api={this.api}/>
      </Provider>
      </div>
    );
  }
}

export default App;
