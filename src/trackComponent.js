import React from 'react';
import Proptypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import { Typography } from '@material-ui/core';
import selectTrackAction from './actions/selectTrackAction';
import { connect } from 'react-redux';

class TrackComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            track: props.track
        };
        console.log(this.state.track);
        this.styles = {
            media: {
                height: 100,
                width: 100,
                display: 'flex',
                objectFit: 'cover',
                marginLeft: '45%'
            },
            card: {
                justifyContent: 'center',
                alignItems: 'center',
            }
        }   
    }

    render() {
        const track = this.state.track;
        return (
            <Card style={this.styles.card}>
                <CardHeader
                    title={track.trackName}
                />
                <CardContent>
                    <Typography>
                        {track.artistName}
                    </Typography>
                </CardContent>
                <CardMedia
                    image={track.artworkUrl100}
                    title={track.artworkUrl100}
                    style={this.styles.media}
                />
                <CardContent>
                    <Typography>
                        Price: ${track.trackPrice}
                        <br/>
                        Duration: {this.msToMins(track.trackTimeMillis)}
                        <br/>
                        Released: {new Date(track.releaseDate).toDateString()}
                    </Typography>
                </CardContent>
                <CardActions style={{justifyContent: 'center'}}>
                    <Button 
                    size="small" 
                    color="primary"
                    onClick={() => {this.goToTrackDetail(track.trackViewUrl)}}
                    >
                    More Details
                    </Button>
                    <Button 
                    size="small" 
                    color="primary"
                    onClick={() => {this.goBack()}}
                    >
                    Back
                    </Button>
                </CardActions>
            </Card>
        );
    }

    goToTrackDetail(url) {
        const win = window.open(url, '_blank');
        win.focus();
    }

    goBack() {
        this.props.selectTrack();
    }

    msToMins(ms) {
        const date = new Date(ms);
        return `${date.getUTCMinutes()}:${date.getUTCSeconds()}`;
    }
}

TrackComponent.Proptypes = {
    track: Proptypes.object.isRequired,
    selectTrack: Proptypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
    selectTrack: track => dispatch(selectTrackAction())
})

export default connect(
    null,
    mapDispatchToProps
)(TrackComponent)