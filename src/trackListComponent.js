import React from 'react';
import Proptypes from 'prop-types';
import { connect } from 'react-redux'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import selectTrackAction from './actions/selectTrackAction';

class TrackListComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    componentDidMount() {
        this.setState({});
        this.props.api.fetchTrackList()
        .then(data => 
            {
                console.log(data);
              this.setState({trackList: data.results})
            }
        );
    }

    setTrack(trackId) {
        this.props.selectTrack(trackId);
    }

    render() {
        if(this.state.trackList){
            return(
            <div>
            <h1>Rock Tracks</h1>
            <h4>Made by Aman Kataria for Scottish Power</h4>
             <List>
                 {this.state.trackList.map(track => {
                     return (
                     <ListItem key={track.trackId} dense button onClick={() => {this.setTrack(track)}}>
                        <ListItemAvatar>
                          <Avatar src={track.artworkUrl100} />
                        </ListItemAvatar>
                        <ListItemText 
                            primary={track.trackName}
                            secondary={
                                <React.Fragment>
                                    <Typography component="span" color="textPrimary">
                                        {track.artistName}
                                    </Typography>
                                    $ {track.trackPrice}
                                </React.Fragment>
                            }
                        />
                     </ListItem>
                    ) 
                 })}
             </List>
             </div>   
            )
        }
        else {
            return null;
        }
    }
}

TrackListComponent.Proptypes = {
    api : Proptypes.object.isRequired,
    selectTrack: Proptypes.func.isRequired
}

const mapDispatchToProps = dispatch => ({
    selectTrack: track => dispatch(selectTrackAction(track))
  })

export default connect(
    null,
    mapDispatchToProps
)(TrackListComponent)