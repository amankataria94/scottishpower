import React from 'react';
import RockTrackComponent from './rockTracksComponent';
import TrackListComponent from './trackListComponent';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import configureStore from 'redux-mock-store';

describe.only('<RockTracksComponent>', () => {
    const mockAPI = { fetchTrackList: () =>{return Promise.resolve()}}
    const mockStore = configureStore([]);
    const initialState = {selectedTrack:{}};
    const store = mockStore(initialState);

    Enzyme.configure({ adapter: new Adapter() })

    it('Should contain TrackListComponent on initialState', () => {
        const wrapper = Enzyme.shallow(
            <RockTrackComponent store={store} api = {mockAPI}/>
        ).dive()

        expect(wrapper.find(TrackListComponent).length).toBe(1);
    })
})