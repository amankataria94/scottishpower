export default function(trackId) {
    return {
        type: 'SET_SELECTED',
        track: trackId
    }
}